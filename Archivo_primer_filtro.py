# -*- coding: utf-8 -*-
"""
Created on Wed May 27 22:21:09 2020

@author: NATALIA
"""

import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
from linearFIR import filter_design
import scipy.signal as signal

#Se carga la señal a analizar

filename = './respiratory-sound-database/Respiratory_Sound_Database/audio_and_txt_files/101_1b1_Al_sc_Meditron.wav'
#filename = './example_data/101_1b1_Pr_sc_Meditron.wav'
senal, Fmuestreo = librosa.load(filename) #señal y frecuencia de muestreo de la señal

def filtrado(senal, Fmuestreo):
    #Diseño de filtros, rango de interés 100-1000Hz ???????????????(Corroborarlo)
    #Filtro pasa bajas de 1000Hz
    order, lowpass = filter_design(Fmuestreo, locutoff = 0, hicutoff = 1000, revfilt = 0); 
    #Filtro pasa altas de 100Hz
    order, highpass = filter_design(Fmuestreo, locutoff = 100, hicutoff = 0, revfilt = 1);
    #Aplicacion de los filtros a la señal
    #Se aplica filtro pasa altas
    senal_hp = signal.filtfilt(highpass, 1, senal);
    #Se aplica filtro pasa bajas
    senal_bp = signal.filtfilt(lowpass, 1, senal_hp);
    #debe hacerse esta conversion para evitar un error que saca al grqaficar
    senal_filtrada = np.asfortranarray(senal_bp) 
    return senal_filtrada, Fmuestreo
    
#Llamado de la función para filtrar y almacenado en variables
senal_filtrada, sr = filtrado(senal,Fmuestreo) 

#Graficado de la señal original y la señal filtrada
plt.figure(figsize=(14, 5));
librosa.display.waveplot(senal, sr=sr);
librosa.display.waveplot(senal_filtrada, sr=sr);
plt.title("Señale origila y filtrada")
plt.show()
    