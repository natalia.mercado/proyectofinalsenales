# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 23:27:43 2020

@author: FiorellaRF
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import mannwhitneyu

df = pd.read_csv("datos_finales.csv")
sanos= df.loc[df.loc[:,"Clasificacion"]=='Sano']
crepitancias= df.loc[df.loc[:,"Clasificacion"]=='Crepitancia']
sibilancia=df.loc[df.loc[:,"Clasificacion"]=='Sibilancia']
c_s=df.loc[df.loc[:,"Clasificacion"]=='Ambas']


sanos_v = sanos.loc[:,"Varianza"]
sanos_r = sanos.loc[:,"Rango"]
sanos_smm = sanos.loc[:,"SMM"]
sanos_pe = sanos.loc[:,"PE"]

crepitancias_v = crepitancias.loc[:,"Varianza"]
crepitancias_r = crepitancias.loc[:,"Rango"]
crepitancias_smm = crepitancias.loc[:,"SMM"]
crepitancias_pe = crepitancias.loc[:,"PE"]

sibilancia_v = sibilancia.loc[:,"Varianza"]
sibilancia_r = sibilancia.loc[:,"Rango"]
sibilancia_smm = sibilancia.loc[:,"SMM"]
sibilancia_pe = sibilancia.loc[:,"PE"]

c_s_v = c_s.loc[:,"Varianza"]
c_s_r = c_s.loc[:,"Rango"]
c_s_smm = c_s.loc[:,"SMM"]
c_s_pe = c_s.loc[:,"PE"]

#Histogramas

plt.subplot(2, 2, 1)
sanos_v.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Varianza sano')

plt.subplot(2, 2, 2)
sanos_r.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Rango sano')

plt.subplot(2, 2, 3)
sanos_smm.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Suma medias móviles sano')

plt.subplot(2, 2, 4)
sanos_pe.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Promedio espectro sano')
plt.show()

plt.subplot(2, 2, 1)
crepitancias_v.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Varianza crepitancia')

plt.subplot(2, 2, 2)
crepitancias_r.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Rango crepitancia')

plt.subplot(2, 2, 3)
crepitancias_smm.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Suma medias móviles crepitancia')

plt.subplot(2, 2, 4)
crepitancias_pe.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Promedio espectro crepitancia')
plt.show()

plt.subplot(2, 2, 1)
sibilancia_v.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Varianza sibilancia')

plt.subplot(2, 2, 2)
sibilancia_r.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Rango sibilancia')

plt.subplot(2, 2, 3)
sibilancia_smm.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Suma medias móviles sibilancia')

plt.subplot(2, 2, 4)
sibilancia_pe.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Promedio espectro sibilancia')
plt.show()

plt.subplot(2, 2, 1)
c_s_v.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Varianza crepitancia y sibilancia')

plt.subplot(2, 2, 2)
c_s_r.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Rango crepitancia y sibilancia')

plt.subplot(2, 2, 3)
c_s_smm.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Suma medias móviles crepitancia y sibilancia')

plt.subplot(2, 2, 4)
c_s_pe.hist()
plt.xlabel('Valor de indice')
plt.ylabel('Cantidad')
plt.title('Promedio espectro crepitancia y sibilancia')
plt.show()

#Prueba de U Mann-Whitney

UMW_SCV= mannwhitneyu(sanos_v, crepitancias_v)
print('Prueba de U Mann-Whitney SCV: ', UMW_SCV)

UMW_SCR= mannwhitneyu(sanos_r, crepitancias_r)
print('Prueba de U Mann-Whitney SCR: ', UMW_SCR)

UMW_SCSMM= mannwhitneyu(sanos_smm, crepitancias_smm)
print('Prueba de U Mann-Whitney SCSMM: ', UMW_SCSMM)

UMW_SCPE= mannwhitneyu(sanos_pe, crepitancias_pe)
print('Prueba de U Mann-Whitney SCPE: ', UMW_SCPE)

UMW_SSV= mannwhitneyu(sanos_v, sibilancia_v)
print('Prueba de U Mann-Whitney SSV: ', UMW_SSV)

UMW_SSR= mannwhitneyu(sanos_r, sibilancia_r)
print('Prueba de U Mann-Whitney SSR: ', UMW_SSR)

UMW_SSSMM= mannwhitneyu(sanos_smm, sibilancia_smm)
print('Prueba de U Mann-Whitney SSSMM: ', UMW_SSSMM)

UMW_SSPE= mannwhitneyu(sanos_pe, sibilancia_pe)
print('Prueba de U Mann-Whitney SSPE: ', UMW_SSPE)

UMW_SCSV= mannwhitneyu(sanos_v, c_s_v)
print('Prueba de U Mann-Whitney SCSV: ', UMW_SCSV)

UMW_SCSR= mannwhitneyu(sanos_r, c_s_r)
print('Prueba de U Mann-Whitney SCSR: ', UMW_SCSR)

UMW_SCSSMM= mannwhitneyu(sanos_smm, c_s_smm)
print('Prueba de U Mann-Whitney SCSSSMM: ', UMW_SCSSMM)

UMW_SCSPE= mannwhitneyu(sanos_pe, c_s_pe)
print('Prueba de U Mann-Whitney SCSPE: ', UMW_SCSPE)

