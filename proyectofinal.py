import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
from linearFIR import filter_design
import scipy.signal as signal
import pywt
from scipy.io.wavfile import write
import os;
import glob
import pandas as pd

<<<<<<< HEAD
#Se carga una señal de prueba para analizarla
filename = r'Respiratory_Sound_Database/Respiratory_Sound_Database/audio_and_txt_files/101_1b1_Al_sc_Meditron.wav'
#filename = './example_data/101_1b1_Pr_sc_Meditron.wav'
senal, Fmuestreo = librosa.load(filename) #señal y frecuencia de muestreo de la señal
=======
#%% Carga de un archivo de prueba
>>>>>>> 189959c21f7e95af364ff29d9da3adf412014bd8

# filename = './respiratory-sound-database/Respiratory_Sound_Database/audio_and_txt_files/101_1b1_Al_sc_Meditron.wav'
# #filename = './example_data/101_1b1_Pr_sc_Meditron.wav'
# senal, Fmuestreo = librosa.load(filename) #señal y frecuencia de muestreo de la señal

#%% Se muestra la señal cargada (PRUEBA!)

# print(senal.shape)
# print(Fmuestreo)

# plt.figure(figsize=(8, 4));
# librosa.display.waveplot(senal, sr=Fmuestreo);
# plt.title("Señal original")
# plt.show()

#%%   Se cargan TODOS los archivos de la carpeta de la base de datos
ruta_archivos = './respiratory-sound-database/Respiratory_Sound_Database/audio_and_txt_files/'

#listando los archivos en el directorio
archivos=os.listdir(ruta_archivos)

#Se crean Variables para almacenar las rutas
archivos_anotacion = []
archivos_audio = []

for file in archivos:
    if file.endswith(".txt"):
        archivos_anotacion.append(file)
    else:
        archivos_audio.append(file)

archivos_anotacion = glob.glob(ruta_archivos +'/*.txt',recursive = True)
archivos_audio = glob.glob(ruta_archivos +'/*.wav',recursive = True)

def abrir_archivo(ruta_paciente_anotacion,ruta_paciente_audio):
    '''
    Parametros
    ----------
    ruta_archivo_anotaciones : ruta donde se encuentra el archivo de anotaciones de interés
    ruta_archivo_audio : ruta donde se encuentra el archivo de audio de interés
    
    Retorna
    -------
    anotacion : TYPE: array of float64, Variable que contiene la información del 
    senal : TYPE: array of float64, Señal que interesa analizar
    Fmuestreo : Frecuencia de muestreo de la señal a analizar.
    '''
    anotacion = np.loadtxt(ruta_paciente_anotacion)
    senal, Fmuestreo = librosa.load(ruta_paciente_audio)
    
    return anotacion,senal,Fmuestreo

#%% Definición del Filtro lineal: filtro pasabanda

def filtradoPasaBanda(senal, Fmuestreo):
    '''
    Parámetros
    ----------
    senal : archivo.wav, de la señal que quiere analizarse (original)
    Fmuestreo : float, Frecuencia de muestreo de a señal ingresada.

    Retorna
    -------
    senal_filtrada : señal que se encuentra filtrada entre 100-1000Hz
    '''
    #Diseño del Filtro pasa bajas de 1000Hz
    order, pasa_bajas = filter_design(Fmuestreo, locutoff = 0, hicutoff = 1000, revfilt = 0); 
    #Diseño del Filtro pasa altas de 100Hz
    order, pasa_altas = filter_design(Fmuestreo, locutoff = 100, hicutoff = 0, revfilt = 1);
    
    #Aplicacion filtro pasa altas
    senal_hp = signal.filtfilt(pasa_altas, 1, senal);
    #Aplicacion filtro pasa bajas
    senal_filtrada = signal.filtfilt(pasa_altas, 1, senal_hp);
    
    #Debe hacerse esta conversion para evitar un error que saca al grqaficar
    senal_filtrada = np.asfortranarray(senal_filtrada) 
    
    return senal_filtrada

#%% Probando el filtrado pasabanda en la señal de prueba (PRUEBA!)
    
# senal_filtrada = filtradoPasaBanda(senal,Fmuestreo) 

# #Graficado de la señal original y la señal filtrada
# plt.figure(figsize=(14, 5));
# librosa.display.waveplot(senal, sr=Fmuestreo, label= 'Señal original');
# librosa.display.waveplot(senal_filtrada, sr=Fmuestreo, label= 'Señal filtrada');
# plt.title("Señal original y filtrada")
# plt.legend()
# plt.show()

# write('test1.wav', Fmuestreo, senal_filtrada)
    
#%% Definición de funciones necesarias para Filtro Wavelet

def wthresh(coeff,thr):
    y   = list();
    s = wnoisest(coeff);
    for i in range(0,len(coeff)):
        y.append(np.multiply(coeff[i],np.abs(coeff[i])>(thr*s[i])));
    return y;
    
def thselect(signal):
    Num_samples = 0;
    for i in range(0,len(signal)):
        Num_samples = Num_samples + signal[i].shape[0];
    
    thr = np.sqrt(2*(np.log(Num_samples)))
    return thr

def wnoisest(coeff):
    stdc = np.zeros((len(coeff),1));
    for i in range(1,len(coeff)):
        stdc[i] = (np.median(np.absolute(coeff[i])))/0.6745;
    return stdc;

#%% Probando Filtro Wavelet (PRUEBA!)
    
# LL = int(np.floor(np.log2(senal.shape[0])));
# coeff = pywt.wavedec(senal, 'db36', level=LL );

# thr = thselect(coeff);
# coeff_t = wthresh(coeff,thr);

# #aplicacion del filtro prueba
# senal_fil = pywt.waverec(coeff_t, 'db36');
# senal_fil= senal_fil[0:senal.shape[0]];
# senal_rest = np.squeeze(senal - senal_fil);

# #Graficación de las señales
# librosa.display.waveplot(senal, sr=Fmuestreo, label= 'Señal original');
# librosa.display.waveplot(senal_fil, sr=Fmuestreo, label= 'Señal wavelet');
# librosa.display.waveplot(senal_rest, sr=Fmuestreo, label= 'Señal sin ruido cardíaco');
# plt.title("Comparación de las señales")
# plt.legend()
# plt.show()

# write('test2.wav', Fmuestreo, senal_rest)

#%% Deffinición de funcion que hace llamado a ambos filtros

def Pre_Procesamiento(senal, Fmuestreo):
    '''
    Parámetros
    ----------
    senal : archivo.wav, de la señal que quiere analizarse
    Fmuestreo : float, Frecuencia de muestreo de a señal ingresada.

    Retorna
    -------
    senal_filtrada : señal que se encuentra sin ruido cardíaco y entre 100-1000Hz
    '''
    LL = int(np.floor(np.log2(senal.shape[0])));
    coeff = pywt.wavedec(senal, 'db36', level=LL );
    thr = thselect(coeff);
    coeff_t = wthresh(coeff,thr);
    
    #Aplicación del filtro Wavelet
    senal_fil = pywt.waverec(coeff_t, 'db36');
    senal_fil= senal_fil[0:senal.shape[0]];
    
    #Señal a filtrada: sin ruido cardíaco
    señal_sin_ruido = np.squeeze(senal - senal_fil);
    
    #Aplicación del filtro pasa banda a la señal sin ruido cardíaco
    senal_filtrada = filtradoPasaBanda(señal_sin_ruido, Fmuestreo) 
    
    return senal_filtrada

#senal_filtrada = Pre_Procesamiento(senal, Fmuestreo)

#%% Llamado para probar el funcionamiento de la función de preprocesamiento (PRUEBA!)

# plt.figure(figsize=(8, 4));
# librosa.display.waveplot(senal, sr=Fmuestreo, label= 'Señal original');
# librosa.display.waveplot(senal_filtrada, sr=Fmuestreo, label= 'Señal Filtrada: sin ruido Cardíaco y en la banda de interés');
# plt.title("Señal original y filtrada")
# plt.legend()
# plt.show()

# write('test1.wav', Fmuestreo, senal_filtrada)

#%% Implementación de la clasificación de los archivos cargados segun:

<<<<<<< HEAD
#Punto 5

def ciclosrespiratorios(senal_filrada, path_archivo):
    path= open(path_archivo)
    read_path= path.readlines()
    print(read_path)
=======
    #Sanos: Ciclo Normal
    #Crepitancias: Crepitancia
    #Sibilancias: Sibilancia
    #Crepitancias y Sibilancias: Crepitancia y Sibilancia

def clasificar(anotacion, senal_filtrada, Fmuestreo):   
    '''   
    Parámetros
    ----------
    anotacion : TYPE: array
        archivo de anotaciones que ya fue cargado de la señal de interés.
    senal_filtrada : TYPE array
        señal que ya ha sido pasada por los filtros.

    Returns
    -------
    lista_ciclos : TYPE: lista. donde se encuentra la información de los ciclos
    lista_clasificacion : TYPE: lista, donde se encuentra la clasifiación de los ciclos
    df : TYPE: dataframe, de las dos variables anteriores
    '''
    ciclo = range(0,anotacion.shape[0])

    #Creación de listas a implementar en la clasificación.
    ciclos_crepitancias = []; ciclos_sibilancias = []
    ciclos_sano = []; ciclos_no_sano = []
    lista_ciclos = []; lista_clasificacion = [] 
    
    i = 0;
    for i in ciclo:
        
        #Conversión del tiempo inicial a muestra inicial
        muestra_inicial = np.int((anotacion[i,0])*Fmuestreo)
        
        #Conversión del tiempo final a muestra final
        muestra_final = np.int((anotacion[i,1])*Fmuestreo)
        
        #Se selecciona de la señal cargada, el ciclo correspondiente en el for
        cicloi = senal_filtrada[muestra_inicial:muestra_final]
        
        #Se adiciona a la lista ciclos, el ciclo actual en el for
        lista_ciclos.append(cicloi)
        
        #Clasificación de los ciclos: sanos/crepitancia/sibilanci/crepitanciySibilancia
        if ((anotacion[i,2] == 0) and (anotacion[i,3] ==0)):
            ciclos_sano.append(i)
            lista_clasificacion.append('Sano')
        elif ((anotacion[i,2] == 1) and (anotacion[i,3] ==0)):
            ciclos_crepitancias.append(i)
            lista_clasificacion.append('Crepitancia')
        elif ((anotacion[i,2] == 0) and (anotacion[i,3] ==1)):
            ciclos_sibilancias.append(i)
            lista_clasificacion.append('Sibilancia')
        elif ((anotacion[i,2] == 1) and (anotacion[i,3] ==1)):
            ciclos_no_sano.append(i)
            lista_clasificacion.append('Ambas')
            
    #Creación de un dataframe para contener la informacipon de los ciclos de una señal (UN paciente)            
    #df = pd.DataFrame({'Ciclo':lista_ciclos, 'Clasificacion':lista_clasificacion})
    
    return lista_ciclos, lista_clasificacion

#%% Se elige uno de los archivos que se han cargado para probar funcionalidad del código
    
# lista_ciclos, lista_clasificacion, df = clasificar(anotacion, senal_filtrada)

# print("Ciclos: ", lista_ciclos)
# print("Clasificacion:" , lista_clasificacion)
# print("Dataframe del paciente: ", df)

#%% Se definen Funciones que realizarán la estadística al ciclo

def indices(ciclo):
    ''' 
    Funcion que retorna los parametros estadísticos solicitados para un ciclo
    Recibe: ciclo (listado con los datos del ciclo)
    Retorna: los 4 parámetros solicitados.
    '''
    varianza = np.var(ciclo)
    rango = np.abs(np.max(ciclo) - np.min(ciclo))
    muestras = 800
    corrimiento = 100
    recorrido = np.arange(0,len(ciclo)-muestras, corrimiento)
    promedio = []
    for i in recorrido: 
        promedio.append(np.mean([ciclo[i:i+muestras]]))
    promedio.append(np.mean([ciclo[recorrido[len(recorrido)-1]:]]))
    smm = np.mean(promedio)
    
    f, pxx = signal.welch(ciclo)
    promedio_espectro = np.mean(pxx)
        
    return varianza, rango, smm, promedio_espectro

# varianza, rango, smm, promedio_espectro = indices(lista_ciclos[0])
# print("varianza del ciclo: ", varianza)
# print("Rango del ciclo: ", rango)
# print("Suma de Medias Móviles del ciclo:", smm)
# print("Promedio del espectro del ciclo: ", promedio_espectro)

#%% Análisis de todas los datos empleando las demás rutinas

varianzaT = []
rangoT = []
smmT = []
promedio_espectroT = []
classT = []
    

def Analisis_Completo():
    '''  
    Funcion que implementa el análisis a todos los datos.
    '''
    for i in range(len(archivos_audio)):
        print("Análisis "+str(i+1)+"/"+str(len(archivos_audio))+ "" + archivos_audio[i])
        anotacion, senal, Fmuestreo = abrir_archivo(archivos_anotacion[i],archivos_audio[i])
        senal_filtrada = Pre_Procesamiento(senal, Fmuestreo)
        lista_ciclos, lista_clasificacion = clasificar(anotacion, senal_filtrada, Fmuestreo)
        
        for i in range(len(lista_clasificacion)):
            x = lista_clasificacion[i]
            classT.append(x)
    
        for i in range(len(lista_ciclos)):
            varianza, rango, smm, promedio_espectro = indices(lista_ciclos[i])
            varianzaT.append(varianza)
            rangoT.append(rango)
            smmT.append(smm)
            promedio_espectroT.append(promedio_espectro)
     
    datos_finales = pd.DataFrame({'Clasificacion':classT, 'Varianza':varianzaT, 'Rango': rangoT, 'SMM': smmT, 'PE':promedio_espectroT})
    return datos_finales


#%%
#probemos resultados para los primeros 5 datos

archivos_audio = archivos_audio[0:5]

datos_finales = Analisis_Completo()
print("A continuación se presentan la informacion de cada uno de los ciclos: ", datos_finales)
   



    
    
    
    


>>>>>>> 189959c21f7e95af364ff29d9da3adf412014bd8

    
    
path_archivo= 'Respiratory_Sound_Database/Respiratory_Sound_Database/audio_and_txt_files/101_1b1_Al_sc_Meditron.txt'

ciclosrespiratorios(senal_filtrada, path_archivo)
